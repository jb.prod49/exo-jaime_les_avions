import json
import pandas as pd
from datetime import datetime
from datetime import timedelta
from dash import Dash, html, dash_table, dcc
from dash.dependencies import Input, Output, State

chemin_fichier_json = 'ressources/data_avion.json'

df = pd.read_json(chemin_fichier_json)

app = Dash(__name__)

#Question 8 - Le voyage le plus long avec sa durée.
voyage_plus_long=df[['airline','depart','destination','duration']][df.duration == df['duration'].max()]


#App layout
app.layout = html.Div([
    html.H1(children='Le voyage le plus long avec sa durée', style={'textAlign':'center'}),
        dash_table.DataTable(
        id='datatable',
        columns=[{'name': col, 'id': col} for col in df.columns],
        data=voyage_plus_long.to_dict('records'),
        style_cell={'textAlign': 'left'},
        style_data={
         'color': 'black',
         'backgroundColor': 'white'
         },
         style_header={ 'border': '1px solid black' },
        hidden_columns=[ 'route','date_heure_arrive','date_heure_depart','price','total_stop', 'additional_info' ],
        page_size=30,

       # Enable filtering
    
       sort_action='native',
        
    ),
])


if __name__ == '__main__':
    app.run_server(debug=True)
