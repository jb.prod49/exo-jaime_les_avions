import json
import pandas as pd
from datetime import datetime
from datetime import timedelta
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import plotly.graph_objs as go
from datapackage import Package
import numpy as np
import folium

# --------------------------------------------#
# ------------ Carte Inde trajets ------------#
# --------------------------------------------#

# carte à ouvrir via un navigateur : airports_map_with_routes.html


chemin_fichier_json = 'ressources/data_avion.json'
chemin_fichier_csv = 'ressources/airport-codes_csv.csv'

df_routes = pd.read_json(chemin_fichier_json)
df_airports = pd.read_csv(chemin_fichier_csv)
df_airports[['Longitude', 'Latitude']] = df_airports['coordinates'].str.split(', ', expand=True).astype(float)

codes_iata_json = set()
for route in df_routes['route']:
    if isinstance(route, list):  
        codes_iata_json.update(route)

codes_iata_list = list(codes_iata_json)
matching_airports = df_airports[df_airports['iata_code'].isin(codes_iata_list)]

m = folium.Map(location=[20.5937, 78.9629], zoom_start=3)

marker_group = folium.FeatureGroup(name='Marqueurs')
line_group = folium.FeatureGroup(name='Lignes')

for index, row in matching_airports.iterrows():
    marker = folium.Marker([row['Latitude'], row['Longitude']], popup=row['name'])
    marker.add_to(marker_group)

for route in df_routes['route']:
    if isinstance(route, list) and len(route) > 1:
        points = []
        for airport_code in route:
            airport = matching_airports[matching_airports['iata_code'] == airport_code].iloc[0]
            points.append([airport['Latitude'], airport['Longitude']])
        line = folium.PolyLine(points, color="blue", weight=2.5, opacity=1)
        line.add_to(line_group)

marker_group.add_to(m)
line_group.add_to(m)

folium.LayerControl(collapsed=False).add_to(m)
m.save('airports_map_with_routes.html')


# pour pouvoir finir cette partie de l'exercice, 
# il faut alouer un plus gros budget sur le matériel informatique.