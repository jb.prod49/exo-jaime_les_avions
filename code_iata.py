import json
import csv
import pandas as pd

# ------------------------------------------#
# -------- ressource a télécharger ---------#
# ------------------------------------------#

# https://datahub.io/core/airport-codes

# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#

def process_row(row, list_codes_iata):
    iata_value = row.get('iata_code')
    if iata_value in list_codes_iata:
        temp = {'code_iata': iata_value, 'municipality': row.get('municipality'), 'airport': row.get('name')}
        return temp
    else:
        return None

# --------------------------#
# -------- Le code ---------#
# --------------------------#

with open('ressources/data_avion.json', 'r') as file:
    data = json.load(file)
    df = pd.DataFrame(data)
    
with open('ressources/airport-codes_json.json', 'r') as file:
    data = json.load(file)
    df_iata = pd.DataFrame(data)

exploded_df = df.explode('route')
list_codes_iata = exploded_df['route'].unique()
list_codes_iata = [code for code in list_codes_iata if code is not None]

filtered_data = df_iata.apply(process_row, axis=1, list_codes_iata=list_codes_iata)
filtered_data = filtered_data.dropna()

json_data = filtered_data.to_json(orient='records')
chemin_fichier_json = 'ressources/tab_iata.json'

with open(chemin_fichier_json, 'w') as fichier_json:
    fichier_json.write(json_data)

df_filtered = pd.read_json(chemin_fichier_json)
print(df_filtered)
