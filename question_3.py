import json
import pandas as pd
from dash import dcc, html
from datetime import timedelta
from dash import Dash, dcc, html, Input, Output

# --------------------------#
# Une vue avec 2 selects. 
# Dans chacun la liste des aéroports. 
# Quand je sélectionne les deux, affiche la durée moyenne et 
# le prix moyen d’un voyage entre les deux.
# --------------------------#

with open('ressources/data_avion.json', 'r') as file:
    data = json.load(file)
    df = pd.DataFrame(data)
    
# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#   

def trajet(depart, arrivee) :
    trajet = df[(df['depart'] == depart) & (df['destination'] == arrivee)]
    if trajet.empty:
        return "Aucun trajet trouvé pour les aéroports sélectionnés."

    tps_trajet_minutes = trajet['duration'].mean()
    price_trajet_mean = trajet['price'].mean()

    if not pd.isna(tps_trajet_minutes):
        tps_trajet = str(timedelta(seconds=round(tps_trajet_minutes * 60)))
    else:
        tps_trajet = "Durée du trajet non disponible"

    if not pd.isna(price_trajet_mean):
        price_trajet = round(price_trajet_mean, 2)
    else:
        price_trajet = "Prix du trajet non disponible"

    result = f"Durée moyenne du trajet : {tps_trajet}\nPrix moyen du trajet : {price_trajet}"
    return result

# --------------------------#
# -------- Le code ---------#
# --------------------------#

# Utilise les noms de colonnes corrects (depart et destination)
airports = list(set(df['depart'].unique()) | set(df['destination'].unique()))

app = Dash(__name__)

app.layout = html.Div([
    html.Label('Sélectionnez l\'aéroport de départ :'),
    dcc.Dropdown(
        id='airport-depart-dropdown',
        options=[{'label': airport, 'value': airport} for airport in airports],
        multi=False,  
        value=airports[0]  
    ),

    # Deuxième liste déroulante pour l'aéroport de destination
    html.Label('Sélectionnez l\'aéroport de destination :'),
    dcc.Dropdown(
        id='airport-destination-dropdown',
        options=[{'label': airport, 'value': airport} for airport in airports],
        multi=False,  
        value=airports[1]
    ),
    html.Br(),
    html.Div(id='output-duration', style={'margin-top': '20px'})
], style={'margin': 'auto', 'width': '50%'})

@app.callback(
    Output('output-duration', 'children'),
    [Input('airport-depart-dropdown', 'value'),
     Input('airport-destination-dropdown', 'value')]
)
def update_duration(depart, arrivee):
    result = trajet(depart, arrivee)
    return result

if __name__ == '__main__':
    app.run(debug=True)