import json
import pandas as pd
from datetime import datetime
from datetime import timedelta

# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#

def process_row(row):
    row['Date_of_Journey'] = format_date(row['Date_of_Journey'])
    
    if isinstance(row['Route'], str):
        row['Route'] = row['Route'].replace(" → ", " ").split()

    date_heure_depart_str = f"{row['Date_of_Journey']} {row['Dep_Time']}"
    row['date_heure_depart'] = datetime.strptime(date_heure_depart_str, "%Y/%m/%d %H:%M")
    row['Arrival_Time'] = pd.to_datetime(row['Arrival_Time'], errors='coerce')
    duree = dureEnMin(row['Duration'])
    dureeMin = formMin(row['Duration'])
    row['date_heure_arrive'] = row['date_heure_depart'] + duree
    
    temp = {'airline':row['Airline'], 
            'date_heure_depart': row['date_heure_depart'].strftime("%Y/%m/%d %H:%M"),
            'depart': row['Source'],
            'destination':row['Destination'],
            'route': row['Route'],
            'date_heure_arrive': row['date_heure_arrive'].strftime("%Y/%m/%d %H:%M"),
            'duration': dureeMin,
            'total_stop': row['Total_Stops'],
            'additional_info': row['Additional_Info'],
            'price': row['Price']}
    row=temp
    return row

def convertir_heure(chaine_heure):
    return datetime.strptime(chaine_heure, "%H:%M")

def format_date(date_str):
    date_obj = datetime.strptime(date_str, "%d/%m/%Y")
    return date_obj.strftime("%Y/%m/%d")

def dureEnMin(duree_str):
    elements = duree_str.split()
    heures = int(elements[0][:-1]) if elements and elements[0][-1] == 'h' else 0
    minutes = int(elements[1][:-1]) if len(elements) > 1 and elements[1][-1] == 'm' else 0
    duree = timedelta(hours=heures, minutes=minutes)
    return duree

def formMin(duree_str):
    heures, minutes = 0, 0
    if 'h' in duree_str:
        heures = int(duree_str.split('h')[0])
    if 'm' in duree_str:
        minutes = int(duree_str.split('m')[0].split()[-1])
    duree_en_minutes = heures * 60 + minutes
    return duree_en_minutes
    
# --------------------------#
# -------- Le code ---------#
# --------------------------#

df = pd.read_excel("ressources/data-avion-654e41bbacd4a359723456.xlsx")

tab_data = df.apply(process_row, axis=1)

json_data = tab_data.to_json(orient='records')

chemin_fichier_json = 'ressources/data_avion.json'
with open(chemin_fichier_json, 'w') as fichier_json:
    fichier_json.write(json_data)

df = pd.read_json(chemin_fichier_json)
