import json
import pandas as pd
from dash import dcc, html, Dash, Input, Output

with open('ressources/data_avion.json', 'r') as file:
    data = json.load(file)
    df = pd.DataFrame(data)
with open('ressources/tab_iata.json', 'r') as file:
    data = json.load(file)
    df_iata = pd.DataFrame(data)

# --------- A destination de Cochin (COK), la liste des aéroports de départ -----------------------#
# --------- pour chacun, la moyenne des temps de trajet, et du nombre de correspondances. ---------#

# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#

#rechercher tout les trajet a destination de cohin

def searchTrajetDestination(tab_data, destination: str )->list[dict]:
    trajet = []
    temp = {}
    for data in tab_data :
        if data[1] == destination:
            if data[3] is not None:
                if data[3] == 'non-stop':
                    data[3] = 0
                else:
                    data[3] = int(data[3].split(' ')[0])
            temp = {'depart': data[0],'destination': data[1], 'duration': data[2], 'total_stop':data[3]}
            trajet.append(temp)
    return trajet

def moyenne(list_data: list[dict]) -> list[dict]:
    df = pd.DataFrame(list_data)
    moyennes = df.groupby('depart').agg({'duration': 'mean', 'total_stop': 'mean'}).reset_index()
    result = moyennes.to_dict(orient='records')

    return result
    
# --------------------------#
# -------- Le code ---------#
# --------------------------#

tab_data = df[['depart', 'destination', 'duration', 'total_stop']].values.tolist()
destination = 'Cochin'

search = searchTrajetDestination(tab_data, destination)
print(moyenne(search))