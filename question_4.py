import json
import pandas as pd
from datetime import datetime
from datetime import timedelta
from dash import Dash, html, dash_table, dcc
from dash.dependencies import Input, Output, State

# question 4 - Une liste de toutes les destinations de la plus chère à 
# la moins chère (moyenne des prix). 

chemin_fichier_json = 'ressources/data_avion.json'

df = pd.read_json(chemin_fichier_json)

app = Dash(__name__)


liste_destination = df.groupby(['destination','airline'])['price'].mean().reset_index()
 

# App layout
app.layout = html.Div([
    html.H1(children='Classement des destinations par prix moyen', style={'textAlign':'center'}),
    dash_table.DataTable(
        id='datatable',
        columns=[{'name': col, 'id': col} for col in df.columns],
        data=liste_destination.to_dict('records'),
        style_cell={'textAlign': 'left'},
        style_data={
        'color': 'black',
        'backgroundColor': 'white'
        },
        style_header={ 'border': '1px solid black' },
        hidden_columns=['date_heure_depart', 'route','date_heure_arrivee','duration','date_heure_arrive','depart','total_stop', 'additional_info' ],
        page_size=30,

        # Enable filtering
        filter_action='native',
        sort_action='native',
        
    ),
])


if __name__ == '__main__':
    app.run_server(debug=True)
