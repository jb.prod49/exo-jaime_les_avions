import plotly.express as px
import json
import pandas as pd
from datetime import datetime
import dash
from dash import dcc, html

# --------------------------#
# ------- Dash Q5-G --------#
# --------------------------#


# Un graphique avec le temps (dates) en abscisse et le nombre de départ/arrivé en ordonnée, 
# avec deux courbes, une pour les départs et une pour les arrivées, 
# avec une granularité à l’heure près.


chemin_fichier_json = 'ressources/data_avion.json'

with open(chemin_fichier_json, 'r', encoding='utf-8') as file:
    data = json.load(file)

df = pd.DataFrame(data)

df['date_heure_depart'] = pd.to_datetime(df['date_heure_depart'], format='%Y/%m/%d %H:%M')
df['heure'] = df['date_heure_depart'].dt.floor('H')
departs = df.groupby('heure').size().reset_index(name='nombre_departs')

df['date_heure_arrive'] = pd.to_datetime(df['date_heure_arrive'], format='%Y/%m/%d %H:%M')
df['heure'] = df['date_heure_arrive'].dt.floor('H')
arrivees = df.groupby('heure').size().reset_index(name='nombre_arrivees')

fig = px.line()
fig.add_scatter(x=departs['heure'], y=departs['nombre_departs'], mode='lines', name='Départs')
fig.add_scatter(x=arrivees['heure'], y=arrivees['nombre_arrivees'], mode='lines', name='Arrivées')

fig.update_layout(
    xaxis_title='Date et heure',
    yaxis_title='Nombre',
    title='Nombre de départs et d\'arrivées par heure'
)

app = dash.Dash(__name__)
app.layout = html.Div([
    dcc.Graph(figure=fig)
])

if __name__ == '__main__':
    app.run_server(debug=True)