import json
import pandas as pd
from datetime import datetime
import plotly.graph_objs as go


# --------------------------#
# ------- Dash Q5-E --------#
# --------------------------#

# Une liste des compagnies aériennes avec le nombre de vols sur la période.
# Charger les données du fichier JSON

chemin_fichier_json = 'ressources/data_avion.json'
with open(chemin_fichier_json, 'r') as file:
    data = json.load(file)

df = pd.DataFrame(data)

df_cochin = df[df['destination'] == 'Cochin']

depart_avg_duration = {}
depart_correspondences = {}

for index, row in df_cochin.iterrows():
    depart = row['depart']
    duration = row['duration']
    route = row['route']

    if route is not None:
        correspondences = len(route) - 2
    else:
        correspondences = 0

    if depart in depart_avg_duration:
        depart_avg_duration[depart].append(duration)
        depart_correspondences[depart].append(correspondences)
    else:
        depart_avg_duration[depart] = [duration]
        depart_correspondences[depart] = [correspondences]


for depart in depart_avg_duration:
    avg_duration = sum(depart_avg_duration[depart]) / len(depart_avg_duration[depart])
    depart_avg_duration[depart] = avg_duration

for depart in depart_correspondences:
    avg_correspondences = sum(depart_correspondences[depart]) / len(depart_correspondences[depart])
    depart_correspondences[depart] = avg_correspondences

result_df = pd.DataFrame({
    'Aéroport de départ': list(depart_avg_duration.keys()),
    'Moyenne des temps de trajet': list(depart_avg_duration.values()),
    'Nombre moyen de correspondances vers Cochin': list(depart_correspondences.values())
})

table_trace = go.Table(
    header=dict(values=list(result_df.columns),
                fill_color='paleturquoise',
                align='left'),
    cells=dict(values=[result_df[col] for col in result_df.columns],
               fill_color='lavender',
               align='left')
)

fig = go.Figure(data=[table_trace])
fig.show()