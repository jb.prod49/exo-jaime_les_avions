import plotly.express as px
import json
import pandas as pd
from datetime import datetime
import dash
from dash import dcc, html

# --------------------------#
# ------- Dash Q5-I --------#
# --------------------------#

# Un histogramme avec les heures en abscisse et le nombre de départs en ordonnée. 
# Bonus : ajouter le nombre d'arrivées.



chemin_fichier_json = 'ressources/data_avion.json'
df = pd.read_json(chemin_fichier_json)


df['date_heure_depart'] = pd.to_datetime(df['date_heure_depart'], format="%Y/%m/%d %H:%M")
df['date_heure_arrive'] = pd.to_datetime(df['date_heure_arrive'], format="%Y/%m/%d %H:%M")

df['heure_depart'] = df['date_heure_depart'].dt.hour
df['heure_arrivee'] = df['date_heure_arrive'].dt.hour

departs_par_heure = df['heure_depart'].value_counts().sort_index()
arrivees_par_heure = df['heure_arrivee'].value_counts().sort_index()

df_departs = pd.DataFrame({'Heure': departs_par_heure.index, 'Nombre de départs': departs_par_heure.values})
df_arrivees = pd.DataFrame({'Heure': arrivees_par_heure.index, 'Nombre d\'arrivées': arrivees_par_heure.values})

df_merged = pd.merge(df_departs, df_arrivees, on='Heure', how='outer').fillna(0)

fig = px.bar(df_merged, x='Heure', y=['Nombre de départs', 'Nombre d\'arrivées'],
             title='Nombre de départs et d\'arrivées d\'avions par heure',
             labels={'value': 'Nombre', 'variable': 'Type'})
fig.update_layout(barmode='group')
fig.show()
