import json
import pandas as pd
from dash import dcc, html, Dash, Input, Output

# --------- l’aéroport qui sert le plus souvent de correspondance. ---------#

with open('ressources/data_avion.json', 'r') as file:
    data = json.load(file)
    df = pd.DataFrame(data)
with open('ressources/tab_iata.json', 'r') as file:
    data = json.load(file)
    df_iata = pd.DataFrame(data)

# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#

def countIata(dataframe, df_iata) -> dict:
    list_route = dataframe['route'].values.tolist()
    list_iata = df_iata[['code_iata', 'municipality']].values.tolist()
    
    tab_count = {municipality: 0 for _, municipality in list_iata}
    
    for route in list_route:
        if route is not None:
            for code_iata, municipality in list_iata:
                if code_iata in route[1:-1]: 
                    tab_count[municipality] += 1
    return tab_count

# --------------------------#
# -------- Le code ---------#
# --------------------------#

# compter combien de fois sont présents chaque code dans le tableau:
tab_count_iata = countIata(df, df_iata)

app = Dash(__name__)

app.layout = html.Div([
    dcc.Graph(id='destination-graph'),
])

@app.callback(
    Output('destination-graph', 'figure'),
    [Input('destination-graph', 'id')]
)
def update_graph(selected_id):
    # Correction : Appeler countIata avec les données correctes
    count_iata_result = countIata(df, df_iata)

    # Correction : Utiliser les clés et valeurs du dictionnaire résultant
    figure = {
        'data': [
            {'x': list(count_iata_result.keys()), 'y': list(count_iata_result.values()), 'type': 'bar', 'name': 'Correspondances'},
        ],
        'layout': {
            'title': 'Visualisation des correspondances'
        }
    }
    return figure

if __name__ == '__main__':
    app.run_server(debug=True)