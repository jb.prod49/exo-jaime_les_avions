import json
import pandas as pd
from datetime import datetime
from datetime import timedelta
from dash import Dash, html, dash_table, dcc
from dash.dependencies import Input, Output, State

chemin_fichier_json = 'ressources/data_avion.json'

# Q1 - Un tableau filtrable et triable qui affiche toutes les informations.

df = pd.read_json(chemin_fichier_json)

# Convertir la colonne 'route' en chaîne de caractères
df['route'] = df['route'].astype(str)

app = Dash(__name__)

# Mise en page de l'application
app.layout = html.Div([
    html.H1(children='Un tableau filtrable et triable des vols en Inde ',
            style={'textAlign': 'center'}),
    dash_table.DataTable(
        id='datatable',
        columns=[{'name': col, 'id': col} for col in df.columns],
        data=df.to_dict('records'),
        page_size=30,

        # Activer le filtrage
        filter_action='native',
        sort_action='native',
        virtualization=True,
    ),
])

# Exécuter l'application
if __name__ == '__main__':
    app.run(debug=True)
