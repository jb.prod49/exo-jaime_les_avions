import json
import pandas as pd
from datetime import datetime
from datetime import timedelta
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px

# --------------------------#
# -------- Dash Q2 ---------#
# --------------------------#

# Un tableau par compagnie aérienne avec la liste des villes (Nom et code iata) 
# qu’elles desservent.

chemin_fichier_json = 'ressources/data_avion.json'

df = pd.read_json(chemin_fichier_json)
app = Dash(__name__)

def ville_unique(routes):
    unique = set()
    for route_list in routes:
        if route_list is not None and len(route_list) > 0:
            unique.add(route_list[-1])  
    return list(unique)

app.layout = html.Div([
    html.H1("Tableau des Villes Desservies par Compagnie Aérienne"),
    dcc.Dropdown(
        id='dropdown',
        options=[{'label': airline, 'value': airline} for airline in df['airline'].unique()],
        value=df['airline'].unique()[0]  
    ),
    html.Table(id='table')
])

@app.callback(
    Output('table', 'children'),
    [Input('dropdown', 'value')]
)
def creer_tableau(selct_compagnie):
    filtered_data = df[df['airline'] == selct_compagnie]
    liste_villes = filtered_data.groupby('destination')['route'].agg(ville_unique).reset_index()

    contenu_tableau = [html.Tr([html.Th('Nom de la Ville'), html.Th('Code IATA')])]
    for index, row in liste_villes.iterrows():
        cities = ', '.join(row['route'])
        contenu_tableau.append(html.Tr([html.Td(row['destination']), html.Td(cities)]))

    return contenu_tableau

if __name__ == '__main__':
    app.run_server(debug=True)
