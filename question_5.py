import json
import pandas as pd
from dash import dcc, html, Dash, Input, Output

# --------- l’aéroport de destination le plus récurrent ---------#

with open('ressources/data_avion.json', 'r') as file:
    data = json.load(file)
    df = pd.DataFrame(data)

# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#
def countDestination(dataframe: pd.DataFrame) -> list[dict]:
    airports = list(set(dataframe['depart'].unique()) | set(dataframe['destination'].unique()))
    tab_count = []
    
    for airport in airports:
        count = len(dataframe[dataframe['destination'] == airport])
        temp = {'airport': airport, 'nb': count}
        tab_count.append(temp)
    
    return tab_count

# --------------------------------------------#
# -------- Déclaration des fonctions ---------#
# --------------------------------------------#

app = Dash(__name__)

app.layout = html.Div([
    dcc.Graph(id='destination-graph'),
])

@app.callback(
    Output('destination-graph', 'figure'),
    [Input('destination-graph', 'id')]
)
def update_graph(selected_id):
    destination_data = countDestination(df)

    figure = {
        'data': [
            {'x': [entry['airport'] for entry in destination_data], 'y': [entry['nb'] for entry in destination_data], 'type': 'bar', 'name': 'Destinations'},
        ],
        'layout': {
            'title': 'Visualisation des destinations'
        }
    }

    return figure

if __name__ == '__main__':
    app.run_server(debug=True)